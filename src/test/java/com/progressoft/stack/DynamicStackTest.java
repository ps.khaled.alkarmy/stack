package com.progressoft.stack;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.progressoft.stack.execptions.StackException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;


public class DynamicStackTest {
    private Stack stack;

    @BeforeEach
    public void init() {
        stack = new Stack();
    }

    @Test
    public void givenValidStack_whenPush_thenExpectedResult() {
        stack.push(10);
        int expectedResult = 1;
        assertThat(stack.size()).isEqualTo(expectedResult);
        expectedResult = 10;
        assertThat(stack.pop()).isEqualTo(expectedResult);
    }

    @Test
    public void givenValidStack_whenPop_thenExpectedResult() {
        stack.push(10);
        int expectedResult = 10;
        assertThat(stack.pop()).isEqualTo(expectedResult);
        expectedResult = 0;
        assertThat(stack.size()).isEqualTo(expectedResult);
    }

    @Test
    public void givenValidStack_whenPopWithEmptyStack_thenExceptionIsThrown() {
        assertThatExceptionOfType(StackException.class).isThrownBy(() -> stack.pop()).withMessage("Stack is empty");
    }

    @Test
    public void givenValidStack_whenPeek_thenExpectedResult() {
        stack.push(10);
        int expectedResult = 10;
        assertThat(stack.peek()).isEqualTo(expectedResult);
        expectedResult = 1;
        assertThat(stack.size()).isEqualTo(expectedResult);
    }

    @Test
    public void givenValidStack_whenPeekWithEmptyStack_thenExceptionIsThrown() {
        assertThatExceptionOfType(StackException.class).isThrownBy(stack::peek).withMessage("Stack is empty");
    }

    @Test
    public void givenValidStack_whenIsEmpty_thenExceptionIsThrown() {
        assertThat(stack.isEmpty()).isEqualTo(true);
        stack.push(10);
        assertThat(stack.isEmpty()).isEqualTo(false);
        stack.peek();
        assertThat(stack.isEmpty()).isEqualTo(false);
        stack.pop();
        assertThat(stack.isEmpty()).isEqualTo(true);
    }

    @Test
    public void givenValidStack_whenIsEqual_thenExpectedResult() {
        stack.push(10);
        stack.push(10);

        Stack stack2 = new Stack();
        stack2.push(10);
        stack2.push(10);
        assertThat(stack.equals(stack2)).isEqualTo(true);

        stack.push(10);
        assertThat(stack.equals(stack2)).isEqualTo(false);
    }
}
