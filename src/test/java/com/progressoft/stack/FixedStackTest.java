package com.progressoft.stack;

import org.junit.jupiter.api.Test;
import com.progressoft.stack.execptions.StackException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;


//TODO: test hashcode

public class FixedStackTest {
    @Test
    public void givenInvalidLength_whenConstructedFixedStack_thenExceptionIsThrown() {
        assertThatExceptionOfType(StackException.class).isThrownBy(() -> new Stack(-1)).withMessage("Invalid length");
    }

    @Test
    public void givenValidFixedStack_whenSize_thenExpectedResult() {
        int length = 10;
        Stack stack = new Stack(length);
        int expectedResult = 10;
        assertThat(stack.size()).isEqualTo(expectedResult);
    }

    @Test
    public void givenValidFixedStack_whenPush_thenExceptionIsThrown() {
        int length = 5;
        Stack stack = new Stack(length);
        assertThatExceptionOfType(StackException.class).isThrownBy(() ->
        {
            for (int i = 0; i < length + 1; i++) {
                stack.push(i);
            }
        }).withMessage("Stack is Full");
    }

    @Test
    public void givenValidFixedStack_whenPush_thenExpectedResult() {
        int length = 5;
        Stack stack = new Stack(length);
        stack.push(10);
        int expectedResult = 10;
        assertThat(stack.pop()).isEqualTo(expectedResult);
    }

    @Test
    public void givenValidFixedStack_whenPop_thenExceptionIsThrown() {
        int length = 5;
        Stack stack = new Stack(length);

        for (int i = 0; i < length; i++) {
            stack.push(i);
        }

        assertThatExceptionOfType(StackException.class).isThrownBy(() -> {
            for (int i = 0; i < length + 1; i++) {
                stack.pop();
            }
        }).withMessage("Stack is empty");

    }

    @Test
    public void givenValidFixedStackWithZeroSize_whenPop_thenExceptionIsThrown() {
        int length = 0;
        Stack stack = new Stack(length);

        assertThatExceptionOfType(StackException.class).isThrownBy(stack::pop).withMessage("Stack is empty");
    }

    @Test
    public void givenValidFixedStackWithZeroSize_whenPeek_thenExceptionIsThrown() {
        int length = 1;
        Stack stack = new Stack(length);
        stack.push(10);
        stack.pop();

        assertThatExceptionOfType(StackException.class).isThrownBy(stack::peek).withMessage("Stack is empty");
    }

    @Test
    public void givenValidFixedStack_whenPeek_thenExpectedResult() {
        int length = 5;
        Stack stack = new Stack(length);
        stack.push(10);
        int expectedResult = 10;
        assertThat(stack.peek()).isEqualTo(expectedResult);
        assertThat(stack.pop()).isEqualTo(expectedResult);
    }

    @Test
    public void givenValidFixedStack_whenIsEmpty_thenExpectedResult() {
        Stack stack = new Stack(10);
        assertThat(stack.isEmpty()).isEqualTo(true);
        stack.push(10);
        assertThat(stack.isEmpty()).isEqualTo(false);
        stack.peek();
        assertThat(stack.isEmpty()).isEqualTo(false);
        stack.pop();
        assertThat(stack.isEmpty()).isEqualTo(true);
    }

    @Test
    public void givenValidStack_whenIsEqual_thenExpectedResult() {
        Stack stack = new Stack(10);
        stack.push(10);
        stack.push(10);

        Stack stack2 = new Stack(10);
        stack2.push(10);
        stack2.push(10);
        assertThat(stack.equals(stack2)).isEqualTo(true);

        stack.push(10);
    }

}
