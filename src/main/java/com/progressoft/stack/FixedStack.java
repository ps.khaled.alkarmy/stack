package com.progressoft.stack;

import com.progressoft.stack.execptions.StackException;

//TODO: implement hashcode
public class FixedStack implements StackStrategy {
    private final int[] stack;
    private int index = 0;

    FixedStack(int length) {
        this.stack = new int[length];
    }

    @Override
    public int peek() {
        if (isEmpty())
            throw new StackException("Stack is empty");
        int i = index - 1;
        return stack[i];
    }

    @Override
    public boolean isEmpty() {
        return index == 0;
    }

    @Override
    public int size() {
        return stack.length;
    }

    @Override
    public int pop() {
        if (isEmpty())
            throw new StackException("Stack is empty");
        return stack[--index];
    }

    @Override
    public void push(int element) {
        if (index == size())
            throw new StackException("Stack is Full");
        stack[index] = element;
        index++;
    }

    public void print() {
        if (isEmpty()) {
            System.out.println("Stack is empty");
            return;
        }
        for (int i = index; i > 0; i--) {
            System.out.println(stack[i - 1]);
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (obj == this) return true;
        if (obj instanceof Stack) {
            Stack stack = (Stack) obj;
            if (stack.size() != this.size()) return false;
            for (int index = 0; index < this.index; ++index) {
                if (stack.pop() != this.pop())
                    return false;
            }
        }
        return true;
    }
}
