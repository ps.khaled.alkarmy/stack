package com.progressoft.stack;

import com.progressoft.stack.execptions.StackException;
//TODO: implement hashcode

public class DynamicStack implements StackStrategy {
    private int[] stack;
    private int index = 0;

    public DynamicStack() {
        this.stack = new int[0];
    }

    @Override
    public int size() {
        return index;
    }

    @Override
    public void push(int element) {
        if (index >= stack.length)
            extendStack();
        stack[index] = element;
        index++;
    }

    @Override
    public int pop() {
        if (isEmpty())
            throw new StackException("Stack is empty");
        int result = stack[--index];
        shrinkStack();
        return result;
    }

    @Override
    public int peek() {
        if (isEmpty())
            throw new StackException("Stack is empty");
        int i = index - 1;
        return stack[i];
    }

    @Override
    public boolean isEmpty() {
        return index == 0;
    }

    private void extendStack() {
        int[] newStack = new int[stack.length + 1];
        System.arraycopy(stack, 0, newStack, 0, stack.length);
        stack = newStack;
    }

    private void shrinkStack() {
        int[] newStack = new int[stack.length - 1];
        System.arraycopy(stack, 0, newStack, 0, stack.length - 1);
        stack = newStack;
    }

    public void print() {
        if (isEmpty()) {
            System.out.println("Stack is empty");
            return;
        }

        for (int i = index; i > 0; i--) {
            System.out.println(stack[i - 1]);
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (obj == this) return true;
        if (obj instanceof Stack) {
            Stack stack = (Stack) obj;
            if (stack.size() != this.size()) return false;
            for (int index = 0; index < stack.size(); index++) {
                if (stack.pop() != this.pop())
                    return false;
            }
        }
        return true;
    }
}
