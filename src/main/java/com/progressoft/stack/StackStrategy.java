package com.progressoft.stack;

public interface StackStrategy {
    int pop();

    void push(int element);

    int size();

    void print();

    int peek();

    boolean isEmpty();
}
