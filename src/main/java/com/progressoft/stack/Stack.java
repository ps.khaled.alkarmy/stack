package com.progressoft.stack;


import com.progressoft.stack.execptions.StackException;

public class Stack implements StackStrategy {

    private StackStrategy stackStrategy;

    public Stack() {
        setStackStrategy(new DynamicStack());
    }

    public Stack(int length) {
        if (length < 0) {
            throw new StackException("Invalid length");
        }
        setStackStrategy(new FixedStack(length));
    }

    private void setStackStrategy(StackStrategy stackStrategy) {
        this.stackStrategy = stackStrategy;
    }

    @Override
    public int pop() {
        return stackStrategy.pop();
    }

    @Override
    public void push(int element) {
        stackStrategy.push(element);
    }

    @Override
    public int size() {
        return stackStrategy.size();
    }

    @Override
    public void print() {
        stackStrategy.print();
    }

    @Override
    public int peek() {
        return stackStrategy.peek();
    }

    @Override
    public boolean isEmpty() {
        return stackStrategy.isEmpty();
    }

    @Override
    public boolean equals(Object obj) {
        return stackStrategy.equals(obj);
    }
}
