package com.progressoft.stack.execptions;

public class StackException extends RuntimeException{
    public StackException(String message) {
        super(message);
    }
}
